#!/usr/bin/perl

use warnings;
use strict;
use Text::Xslate;
use Time::Piece;
use Getopt::Long;
use IPC::System::Simple qw(capturex);

=head1 NAME

make_report.pl

=head1 VERSION

Version 0.01

=head1 AUTHOR

Jeremiah C. Foster

=head1 COPYRIGHT & LICENSE

=over 4

=item Copyright (c) 2021 Jeremiah C. Foster

=back

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301 USA.

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

make_report.pl -h <hours> (an integer)

=head1 DESCRIPTION

This perl script produces a report in html by sending data through an XSlate template.

=cut

=head1 OPTIONS


=head1 TODO


=head1 BUGS

- euros are hard coded, not read from YAML file
- 

=cut

# commandline options
my ($hours);
# get hours this way
# grep -A 16 "Distribute work hours for January 2022" timekeeping.ledger | grep Projects:Available
# my @hours  = capturex(EXIT_ANY, grep -A 16 "Distribute work hours for January 2022" timekeeping.ledger | grep Projects:Available 

# get DLAs like this;
# curl https://lists.debian.org/debian-lts-announce/2021/12/threads.html | grep DLA | wc -l


GetOptions
  (
   "hours=i" => \$hours,
  );
my $rate = 75;


# There is code to read active contributors in yamlette.pl
# NB: checking contributors means looking at the _previous_ month
my @contributors = (
		    "Abhijith PA",
		    "Anton Gladky",
		    "Ben Hutchings",
		    "Chris Lamb",
		    "Emilio Pozuelo Monfort",
		    "Jeremiah Foster",
		    "Lee Garrett",
		    "Markus Koschany",
		    "Roberto C. Sanchez",
		    "Sylvain Beucler",
		    "Thorsten Alteholz",
		    "Utkarsh Gupta",
		   );

my $tx = Text::Xslate->new();
my $t = localtime;
$t = $t->add_months(-1); # last month

# ledger output
# my @projects_available = capturex(EXIT_ANY, "ledger", "-b", "2022-01-01", "-f" $ledger);



my %vars =
  (
   month => $t->fullmonth,
   euros => $hours * $rate,
   total => ($#contributors + 1),
   contribs => \@contributors,
  );
print $tx->render("report_view.tx", \%vars);

1; # End of make_report.pl

