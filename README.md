# LTS reporting and tooling

A replacement for the make_report shell script used in the LTS project.

## Description

This repo contains the following files;
```
.
├── header_view.tx // An XSlate template
├── make_report.pl // perl code
├── README.md      //
└── report_view.tx // template
```
The point of thse files is to gather data and present it in a format
similar, if not identical to the [Freexian monthly report](https://raphaelhertzog.com/2021/11/17/freexians-report-about-debian-long-term-support-october-2021/).

## Installation

## Usage


## License
GPLv3


## TODO

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:d53f17f45329da3e053b2de2add2e828?https://docs.gitlab.com/ee/ci/quick_start/index.html)
***
