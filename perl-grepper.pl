#!/usr/bin/perl

use warnings;
use strict;

=head1 NAME

perl-grepper.pl

=head1 VERSION

Version 0.01

=head1 AUTHOR

Jeremiah C. Foster

=head1 COPYRIGHT & LICENSE

=over 4

=item Copyright (c) 2021 Jeremiah C. Foster

=back

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301 USA.

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

=head1 DESCRIPTION


=cut

=head1 OPTIONS


=head1 TODO


=head1 BUGS


=cut

grep
