#!/usr/bin/perl

use warnings;
use strict;

=head1 NAME

ledger-parser.pl - parse a ledger with Ledger::Parser

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS


=cut

# /home/jeremiah/git/debian-lts/contributors.yml

use Getopt::Long;
use Ledger::Parser;
