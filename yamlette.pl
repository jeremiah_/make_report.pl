#!/usr/bin/perl

use warnings;
use strict;

=head1 NAME

yamlette - Read a yaml file, spit out data

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

=head1 SYNOPSIS

 yamlette.pl [--file = <string>] [--status = <string>]

=cut


use Getopt::Long;
use YAML::Tiny;
use utf8;
binmode STDOUT, ":utf8";

my ($file, $status, $elts, $max, $front);
GetOptions
  (
   "file=s"    => \$file,
   "status=s"  => \$status,
   "elts"      => \$elts,
   "max"       => \$max,
   "front"     => \$front,
  );

$file //= "/home/jeremiah/git/Debian/debian-lts/contributors.yml";
if ($elts) {
  $file = "/home/jeremiah/git/Debian/extended-lts/contributors.yml";
}
print "File is " . $file ."\n";

# Open the config
unless (-e $file) { die "Cannot find file: ", $! };
my $yaml = YAML::Tiny->read( $file );

# Get a reference to the first document
my $config = $yaml->[0];

# read properties directly
my $root = $yaml->[0]->{contributors};

# an array of contributors in the YAML file
my @contribs = ( );
map { push @contribs, $_ } keys %$config;

if ($status) {
  # print contribs status and UTF8 decoded
  print map {
    utf8::decode($_);
    if ($yaml->[0]->{$_}->{status} eq $status) {
      $_ . "\n";
    }
  } sort @contribs;
}

if ($max) {
  print map { $_ . "\tMax hours: ".$config->{$_}->{max}."\n"; } keys %$config;
}

if ($front) {
  print map { $_ . "\tFront desk: ".$config->{$_}->{frontdesk}."\n"; } keys %$config;
}


# print map {
#   $_ ." Max hours\t". $config->{$_}->{max} ."\n\t\t" .  $config->{$_}->{email} . "\n";
# } keys %$config;
